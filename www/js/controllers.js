angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope,$q,$rootScope,$timeout,$ionicHistory,$ionicAnalytics,$cordovaSocialSharing,$cordovaGoogleAnalytics,$localstorage,$translate,$state,$ionicModal,$stateParams,$ionicLoading,$ionicPopup,$http,myConfig, Menu, Page) {
    $rootScope.date=new Date();
    $rootScope.userfields = Array();

    //funciones para construir los select de fecha de nacimiento en "mis-datos"
    $rootScope.dias= Array();
    for(i=1;i<=31;i++){
      $rootScope.dias.push(String(i));
    }

    $rootScope.meses= Array();
    for(i=1;i<=12;i++){
      $rootScope.meses.push(String(i));
    }

    var fecha = new Date();
    var ano = fecha.getFullYear();
    $rootScope.anos= Array();
    for(i=(ano-13);i>=1920;i--){
      $rootScope.anos.push(String(i));
    }

  
    // establecemos el idioma
    $rootScope.lang=$localstorage.get("lang");
    if($rootScope.lang!=undefined){
      $translate.use($rootScope.lang);
      $rootScope.lang=$localstorage.get("lang");
    }
    else{
      $rootScope.lang=$translate.use();
    }
    if($rootScope.lang==undefined || $rootScope.lang==''){
      //por si lo anterior fallara, le asignamos un idioma por defecto
      $rootScope.lang='es';
    }
    //comprobamos si hay información del usuario en localstorage
    $rootScope.user=$localstorage.getObject('user');
    $rootScope.login=false;
    if($rootScope.user.iduser!=undefined){
      $rootScope.login=true;
    }

    //removemos el padding en las promociones premium
    $rootScope.removePadding = function(premium){
      if(premium){
        return "no-padding";
      }
      return "";
    }

    //función para mostrar o salir a una página estática
    $scope.goToPage = function(page){
        $ionicAnalytics.track('Página',page.url);
        if(typeof analytics !== 'undefined'){
            $cordovaGoogleAnalytics.trackView(page.url);
        }
        if(page.external_link){
          window.open(myConfig.URL+'/'+$rootScope.lang+'/info/'+page.url, '_system', 'location=yes');
        }
        else{
          $state.go('app.page',{id:page.id});
        }
    }

    //logeamos con facebook
    $rootScope.loginFacebook = function(idpromocion){
      if(idpromocion==undefined){
        idpromocion=0;
      }
      var deferred = $q.defer();
            facebookConnectPlugin.login(["email", "public_profile"], 
              function(success){
                   facebookConnectPlugin.api(success.authResponse.userID+"/?fields=id,email,age_range,gender,devices", ["email", "public_profile"],
                    function (result) {
                      deferred.resolve(result);
                    },
                    function (error) {
                      deferred.reject(error);
                    });
                   
              },
              function (error) {
                alert(error)
              }
            );
      return deferred.promise;
    }


    //realizamos acción "vincular cuenta de facebook" dentro de una promoción o logeamos con facebook
    $scope.doFacebook = function(idpromocion){
       if(idpromocion==undefined){
          idpromocion=0;
        }
         var login = $rootScope.loginFacebook();
         login.then(function(result){
            var url=myConfig.URL+"/api/v1/usuarioAuth.json";
            $ionicLoading.show({
                content: 'Cargando',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            var fd = new FormData();
            if($rootScope.login){
              fd.append('iduser',$rootScope.user.iduser);
            }
            fd.append('emailfb',result.email);
            fd.append('gender',result.gender);
            fd.append('age_range',result.age_range);
            fd.append('idpromocion', idpromocion);
            fd.append('token', $localstorage.get('token'));
            fd.append('apikey',myConfig.APIKEY);
            
            $http.post(url, fd, {
                transformRequest:angular.identity,
                headers:{'Content-Type':undefined
                }
            }).success(function(data){
                data=JSON.parse(data);
                $ionicLoading.hide();
                if(data.error){
                  var alertPopup = $ionicPopup.alert({
                       template: data.error_msg,
                       title: 'Promokiss',
                       okText: 'Ok'
                  });
                }
                else{
                  $ionicAnalytics.track('Facebook Login',true);
                  if(typeof analytics !== 'undefined'){
                    $cordovaGoogleAnalytics.trackEvent('Facebook Login', 'Logeado con facebook correctamente');
                  }
                  var user = {iduser:data.id,email:data.email,hashcode:data.hashcode};
                  $localstorage.setObject('user',user);
                  $rootScope.user = user;
                  $rootScope.login=true;
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  if(data.idpromocion){
                    $translate(['ACCION_FACEBOOK_OK','ACEPTAR']).then(function (msg) {
                         var alertPopup = $ionicPopup.alert({
                             title: 'Promocion',
                             template: msg.ACCION_FACEBOOK_OK,
                             cssClass: 'stable',
                             buttons: [
                              { text: msg.ACEPTAR,
                                type: 'button-stable', }
                              ]
                           });
                    });
                  }
                  else{
                    $state.go('app.promociones');
                  }
                  
                }
            });
         }) 
      }

    //mostrar una página estática en un modal, lo usamos para aceptar condiciones
    $rootScope.showPage = function(templateUrl) {
       $ionicLoading.show({
              content: 'Cargando',
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
      });
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        Page.get({id:2,lang:$rootScope.lang}).$promise.then(function(data) {
            $scope.page=data;
            $ionicLoading.hide();
            $scope.modal = modal;
            $scope.modal.show();
        });
      });
  }

    $rootScope.showCondicionesPromokiss = function(promo){
      $rootScope.showPage('templates/condiciones_promokiss.html');
    }

   //para las paginas estaticas, esta función también se ejecuta al cambiar de idioma, construimos el menú de la aplicación
   $scope.menu = Array();
   $rootScope.getMenu = function(){
       var menu=Menu.get({lang:$rootScope.lang});
       menu.$promise.then(function(data){
          $scope.menu=data;
       })
   }
   $rootScope.getMenu();

   $scope.isSelected = function(a,b){
      if(a==b){
        return "true";
      }
      return false;
   }
  
    // función que calcular el tiempo que queda dada una fecha
   $rootScope.quedan = function(end_date){
        var fecha=new Date(end_date);
        var hoy=new Date();
        var dias=0;
        var horas=0
        var minutos=0
        var segundos=0
        if (fecha>hoy){
            var diferencia=(fecha.getTime()-hoy.getTime())/1000
            dias=Math.floor(diferencia/86400)
            diferencia=diferencia-(86400*dias)
            horas=Math.floor(diferencia/3600)
            diferencia=diferencia-(3600*horas)
            minutos=Math.floor(diferencia/60)
            diferencia=diferencia-(60*minutos)
            segundos=Math.floor(diferencia);
            if($rootScope.lang=='es'){
              return 'Finaliza en ' + dias + 'd ' + horas + 'h ' + minutos + 'm ';
            }
            else{
              return dias + 'e ' + horas + 'o ' + minutos + 'm amaitzeko';
            }
            
        }
        else{
            if($rootScope.lang=='es'){
              return 'Finalizada';
            }
            else{
              return 'Amaituta';
            }
            
        }
    }
    $scope.range = function(n){
      return new Array(n);
    }
    
    //si una promoción ya está finalizada
    $rootScope.finalizada = function(end_date){
        var fecha=new Date(end_date).getTime();
        var hoy=new Date().getTime();
        if(fecha>hoy){
          return true;
        }
        return false;
    }

    //dice si la fecha de fin de promoción es menor a 30 dias
    $rootScope.menos30dias = function(end_date){
        var fecha = new Date(end_date);
        var addTime = 30 * 86400; //Tiempo en segundos
        fecha.setSeconds(addTime); 
        var fechaSumada=fecha.getTime();
        fecha = new Date().getTime();
        if(fecha>fechaSumada){
          return false;
        }
        return true;
    }

  //se usa para autocompletar las provincias. Creo que ya no se usa, posible DEPRECATED
  $scope.ProvinciasModel = function (query){
      if(query!=''){
           return $http.get(myConfig.URL+"/api/v1/provincias.json?apikey=debug&q="+query);   
      }  
    }

  //para el autocomplete de las poblaciones
  $scope.PoblacionesModel = function (query){
    if(query!=''){
      return $http.get(myConfig.URL+"/api/v1/poblaciones.json?apikey=debug&q="+query);     
    }
  }

  //Evento cuando se comparte una promoción u otro contenido
  $rootScope.share = function(){
    if($rootScope.promocion==undefined){
       $translate(['compartir_promokiss']).then(function (msg) {
            $cordovaSocialSharing
            .share(null, msg.compartir_promokiss, "http://promokiss.com/images/promokiss.png", myConfig.URL) // Share via native share sheet
            .then(function(result) {
            }, function(err) {
              // An error occured. Show a message to the user
            });
        });
        
    }
    else{
      var hashurl="";
      if($rootScope.user!=undefined && $rootScope.user.hashcode!=undefined){
        hashurl="?ref="+$rootScope.user.hashcode;
      }
      $cordovaSocialSharing
      .share(null, $rootScope.promocion.title, myConfig.URL+"/"+$rootScope.promocion.image_share, myConfig.URL+"/"+$rootScope.lang+"/"+$rootScope.promocion.url+hashurl) // Share via native share sheet
      .then(function(result) {
      }, function(err) {
        // An error occured. Show a message to the user
      });
    }
  }
  
  //Actualizamos los datos de acceso del usuario
  $scope.doAccountUpdate = function(){
     $scope.saved=false;
      $scope.saved_msg="";
        var url=myConfig.URL+"/api/v1/updateUsuario.json";
        $ionicLoading.show({
            content: 'Cargando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        var fd = new FormData();
        fd.append('iduser', $rootScope.user.iduser);
        fd.append('lang', $rootScope.lang);
        fd.append('emaila', accountForm.email.value);
        fd.append('passa', accountForm.password.value);
        fd.append('token', $localstorage.get('token'));
        fd.append('apikey',myConfig.APIKEY);
        $http.post(url, fd, {
            transformRequest:angular.identity,
            headers:{'Content-Type':undefined
            }
        }).success(function(data){
          data=JSON.parse(data);
          $ionicLoading.hide();
          if(data.error){
              $rootScope.validation_errors=data.error_msg;
              $rootScope.validation=true;
          }
          else{
              $rootScope.validation_errors="";
              $rootScope.validation=false;
              if(typeof analytics !== 'undefined'){
                $cordovaGoogleAnalytics.trackEvent('Mi cuenta','Datos actualizados');
              }
              $ionicAnalytics.track('Cambios cuenta',true);
              var user = {iduser:data.id,email:data.email,hashcode:data.hashcode,usuario_data:data.usuario_data};
              $rootScope.user=user;
              $localstorage.setObject('user',user);
              $rootScope.login=true;
              $translate('ACCOUNT_SAVED_OK').then(function (msg) {
                  $scope.saved=true;
                  $scope.saved_msg=msg;
              });
            }
        });
    }

    //actualizamos la información personal del usuario
    $scope.doMisDatosUpdate = function(idpromocion){
      if(idpromocion==undefined){
        idpromocion=0;
      }
     $scope.saved=false;
      $scope.saved_msg="";
        var url=myConfig.URL+"/api/v1/updateUsuario.json";
        $ionicLoading.show({
            content: 'Cargando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        var fd = new FormData();
        fd.append('iduser', $rootScope.user.iduser);
        fd.append('apikey',myConfig.APIKEY);
        fd.append('lang',$rootScope.lang);
        if($rootScope.userfields!=undefined){
          fd.append('idpromocion',idpromocion);
          for(var index in $rootScope.userfields) {
            fd.append("user["+index+"]", $rootScope.userfields[index]);
          }
        }
        $http.post(url, fd, {
            transformRequest:angular.identity,
            headers:{'Content-Type':undefined
            }
        }).success(function(data){
          data=JSON.parse(data);
          $ionicLoading.hide();
          if(data.error){
              $rootScope.validation_errors=data.error_msg;
              $rootScope.validation=true;
          }
          else{
            var user = {iduser:data.id,email:data.email,hashcode:data.hashcode,usuario_data:data.usuario_data};
            $rootScope.user=user;
            $localstorage.setObject('user',user);
            $rootScope.validation_errors="";
            $rootScope.validation=false;
           if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.trackEvent('Mi cuenta','Datos actualizados');
            }
            $ionicAnalytics.track('Cambios cuenta',true);
            $translate(['MISDATOS_COMPLETADOS','ACEPTAR']).then(function (msg) {
                 var alertPopup = $ionicPopup.alert({
                     title: 'Promokiss',
                     template: msg.MISDATOS_COMPLETADOS,
                     cssClass: 'stable',
                     buttons: [
                      { text: msg.ACEPTAR,
                        type: 'button-stable'}
                      ]
                   });
            });
              
            }
        });

    }

    //cerrar sesión
    $rootScope.logout = function(){
      $rootScope.login=false;
      $rootScope.user='{}';
      $localstorage.remove("user");
      $state.go("app.promociones");
    }
})
.controller('MisPromocionesCtrl', function($rootScope,$scope,$timeout, $ionicLoading, $ionicNavBarDelegate, MisPromociones, myConfig) {
  var promo_limit=8; //límite de promociones en una llamada
  $rootScope.promocion=undefined;
  $scope.myConfig=myConfig;//myConfig es un objeto con constantes
  $rootScope.myList=Array();//lista de promociones
  $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
  });
  //obtenemos las promociones a través de MisPromociones, un resource definido en services.js
  MisPromociones.all({offset:0,limit:promo_limit,apikey:myConfig.APIKEY,iduser:$rootScope.user.iduser}).$promise.then(function(data) {
          $rootScope.myList=data.promociones;
          $ionicLoading.hide();
  });
  //evento para refrescar las promociones
  $scope.doRefresh = function() {
      $timeout( function() {
          $rootScope.myList=new Array();
          offset=0;
          var myList= MisPromociones.all({offset:0,limit:promo_limit,apikey:myConfig.APIKEY,iduser:$rootScope.user.iduser});
          $scope.moreData=false;
          myList.$promise.then(function(data){
               $scope.moreData=true;
               $rootScope.myList = data.promociones;
        })
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);    
    };
  //eventos para cargar más promociones
  $scope.moreDataCanBeLoaded=function(){
      return $scope.moreData;
  };
  if(offset==undefined){
    var offset=0;
  }
  $scope.moreData=true;
  //se ejecuta al bajar con el scroll
  $scope.loadMore = function() {
        $timeout(function() {
          offset=offset+promo_limit;
          var myList= MisPromociones.all({offset:offset,limit:promo_limit,apikey:myConfig.APIKEY,iduser:$rootScope.user.iduser});
          $scope.moreData=false;
          myList.$promise.then(function(data){
          angular.forEach(data.promociones,function(val){
             $scope.moreData=true;
             $rootScope.myList.push(JSON.parse(angular.toJson(val)));
          })
        })
        $scope.$broadcast('scroll.infiniteScrollComplete'); 
        }, 1000);
    }
})
.controller('MiPromocionDetailCtrl', function($rootScope,$scope, $sce, $ionicAnalytics,$cordovaGoogleAnalytics, $http, $stateParams, $ionicLoading, $ionicPopup, $ionicModal, MiPromocion, myConfig) {
  $rootScope.validated_bool=true;
  $rootScope.validation_errors="";
  $scope.validation=true;
  $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
  });
  var promo= MiPromocion.get({id:$stateParams.Id,lang:$rootScope.lang,iduser:$rootScope.user.iduser});
  promo.$promise.then(function(data){
     $rootScope.promocion=data;
     $scope.complete_fields=data.formulary.complete_fields;
     $ionicAnalytics.track('Visita mi promoción',{id:$rootScope.promocion.id,title:$rootScope.promocion.title});
     $ionicLoading.hide();
     function _waitForAnalytics(){
          if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.startTrackerWithId(myConfig.ANALYTICS);
              $cordovaGoogleAnalytics.trackView($rootScope.promocion.title);
              $cordovaGoogleAnalytics.addCustomDimension(1, $rootScope.lang);
              $cordovaGoogleAnalytics.addCustomDimension(2, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(3, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(4, "Promociones | Promozioak");
              $cordovaGoogleAnalytics.addCustomDimension(5, $rootScope.promocion.id+"|"+$rootScope.promocion.title);
              $cordovaGoogleAnalytics.addCustomDimension(6, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(7, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(8, "(not set)");
          }
          else{
              setTimeout(function(){
                  _waitForAnalytics();
              },250);
          }
      };
      _waitForAnalytics();

  });
  $scope.fields=Array();
  $rootScope.userfields=Array();
  $scope.files=Array();
  $scope.displayShare=true;
  $scope.myConfig=myConfig;
  $scope.to_trusted = function(html_code) {
      return $sce.trustAsHtml(html_code);
  }
  $scope.showImage = function(index) {
    $scope.image = index;
    $scope.showModal('templates/image-popover.html');
  }


 
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
 
  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
  
})
.controller('PromocionesCtrl', function($rootScope,$scope, $ionicHistory, $ionicLoading, $timeout, $cordovaGoogleAnalytics, Promociones,myConfig) {
        function _waitForAnalytics(){
          if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.startTrackerWithId(myConfig.ANALYTICS);
              $cordovaGoogleAnalytics.trackView('Promociones | Promozioak');
              $cordovaGoogleAnalytics.addCustomDimension(1, $rootScope.lang);
              $cordovaGoogleAnalytics.addCustomDimension(2, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(3, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(4, "Promociones | Promozioak");
              $cordovaGoogleAnalytics.addCustomDimension(5, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(6, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(7, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(8, "(not set)");
          }
          else{
              setTimeout(function(){
                  _waitForAnalytics();
              },250);
          }
      };
      _waitForAnalytics();
  $scope.myConfig=myConfig;
  $rootScope.promocion=undefined;
   $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
  });
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  var promo_limit=8;
  $rootScope.list=Array();
  //obtenemos las promociones a través Promociones, un resource definido en services.js
  Promociones.all({offset:0,limit:promo_limit,lang:$rootScope.lang}).$promise.then(function(data) {
          $rootScope.list=data.promociones;
          $ionicLoading.hide();
  });
  //evento para refrescar las promociones
  $scope.doRefresh = function() {
      $timeout( function() {
          $rootScope.list=new Array();
          offset=0;
          var list= Promociones.all({offset:0,limit:promo_limit,lang:$rootScope.lang});
          $scope.moreData=false;
          list.$promise.then(function(data){
               $scope.moreData=true;
               $rootScope.list = data.promociones;
        })
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);    
    };
  //eventos para cargar más promociones
  $scope.moreDataCanBeLoaded=function(){
      return $scope.moreData;
  };
  if(offset==undefined){
    var offset=0;
  }
  $scope.moreData=true;
  $scope.loadMore = function() {
        $timeout(function() {
          offset=offset+promo_limit;
          var list= Promociones.all({offset:offset,limit:promo_limit,lang:$rootScope.lang});
          $scope.moreData=false;
          list.$promise.then(function(data){
          angular.forEach(data.promociones,function(val){
             $scope.moreData=true;
             $rootScope.list.push(JSON.parse(angular.toJson(val)));
          })
        })
        $scope.$broadcast('scroll.infiniteScrollComplete'); 
        }, 1000);
    }
})

.controller('PromocionDetailCtrl', function($rootScope,$scope, $timeout, $sce, $ionicHistory, $translate, $ionicAnalytics, $cordovaGoogleAnalytics, $localstorage, $http, $stateParams, $ionicLoading, $ionicPopup, $ionicModal, Promocion, PromocionURL, ReferAction, myConfig) {
   
  $rootScope.promocion=undefined;
  $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
  });
  $scope.check = function(){
    if($scope.checkCondicionesPromocion==true){
      $scope.checkCondicionesPromocion=false;
    }
    else{
      $scope.checkCondicionesPromocion=true;
    }
  }
  $scope.checkCondicionesPromocion=false;
  $scope.fields=Array();
  $rootScope.userfields=Array();
  $scope.files=Array();
  $scope.displayShare=false;
  $rootScope.validated_bool=true;
  $rootScope.validation_errors="";
  $scope.validation=true;
  $scope.validated = function(idfield){
    $scope.validated_bool=false;
  }
  $scope.myConfig=myConfig;
  var referral='';
  if($stateParams.Id==undefined){
    $rootScope.lang=$stateParams.lang;
    referral=$stateParams.ref;
    var promo= PromocionURL.get({url:$stateParams.url,lang:$stateParams.lang});
  }
  else{
    var promo= Promocion.get({id:$stateParams.Id,lang:$rootScope.lang});
  }
  promo.$promise.then(function(data){
     $rootScope.promocion=data;
     if($rootScope.promocion.conditions!=null){
       $scope.checkCondicionesPromocion=false;
     }
     if(referral!=''){
         //guardamos la información por si el usuario se inscribe en la promoción
         var ref = ReferAction.get({action:'visit', referral:referral, idpromocion:data.id, apikey:myConfig.APIKEY, lang:$rootScope.lang});
          ref.$promise.then(function(data){
               var ref_session={referral:referral,idpromocion:data.id};
               $localstorage.setObject('referral',ref_session);
          })
     }
     $scope.complete_fields=data.formulary.complete_fields;
     $ionicAnalytics.track('Visita promoción',{id:$rootScope.promocion.id,title:$rootScope.promocion.title});
     $ionicLoading.hide();
      function _waitForAnalytics(){
          if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.startTrackerWithId(myConfig.ANALYTICS);
              $cordovaGoogleAnalytics.trackView($rootScope.promocion.title);
              $cordovaGoogleAnalytics.addCustomDimension(1, $rootScope.lang);
              $cordovaGoogleAnalytics.addCustomDimension(2, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(3, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(4, "Promociones | Promozioak");
              $cordovaGoogleAnalytics.addCustomDimension(5, $rootScope.promocion.id+"|"+$rootScope.promocion.title);
              $cordovaGoogleAnalytics.addCustomDimension(6, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(7, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(8, "(not set)");
          }
          else{
              setTimeout(function(){
                  _waitForAnalytics();
              },250);
          }
      };
      _waitForAnalytics();
     
  });
  $scope.to_trusted = function(html_code) {
      return $sce.trustAsHtml(html_code);
  }

  $scope.onValueChanged = function(idfield,value,required){
      //este método es para las respuesta múltiples, enviamos los identificadores de las respuestas separadas por comas
      $scope.fields[idfield]=Array();
      if(!required){
        $scope.validated_bool=true;
      }
      for(var index in value) {
          $scope.fields[idfield][index]=value[index].id;
          $scope.validated_bool=true;
      }
      $scope.fields[idfield].join();
  }

  $scope.doInscripcion = function (){
      var url=myConfig.URL+"/api/v1/inscripcion.json";
      $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
      });
      var fd = new FormData();
      fd.append('lang', $rootScope.lang);
      fd.append('iduser', $rootScope.user.iduser);
      for(var index in $rootScope.userfields) {
        fd.append("user["+index+"]", $rootScope.userfields[index]);
      }
      
      for(var index in $scope.fields) {
        fd.append("custom["+index+"]", $scope.fields[index]);
      }

      fd.append('idpromocion', $rootScope.promocion.id);
      fd.append('apikey',myConfig.APIKEY);
      $http.post(url, fd, {
          transformRequest:angular.identity,
          headers:{'Content-Type':undefined
          }
      }).success(function(data){
          $ionicLoading.hide();
          data=JSON.parse(data);
          if(!data.error){
            $scope.displayShare=true;
             document.getElementById("inscripcionForm").style.display="none";
             var alertPopup = $ionicPopup.alert({
                 template: data.msg,
                 cssClass: 'stable',
                 title: 'Promokiss',
                 buttons: [
                  { text: 'Aceptar',
                    type: 'button-stable'}
                  ]
            });
             $rootScope.user.iduser=data.iduser;
             //miramos si tenemos una recomendación de algún amigo para esta promoción, si es así le añadimos la acción a ese usuario
             var ref_session = $localstorage.getObject('referral');
             if(ref_session!=undefined){
               var ref = ReferAction.get({action:'subscribe', referral:ref_session.referral, idpromocion:$rootScope.promocion.id, apikey:myConfig.APIKEY, lang:$rootScope.lang});
               ref.$promise.then(function(data){
               })
             }
          }
          else{
            $translate('ACEPTAR').then(function (msg) {
                 var alertPopup = $ionicPopup.alert({
                     title: 'Promocion',
                     template: data.error_msg,
                     cssClass: 'stable',
                     buttons: [
                      { text: msg,
                        type: 'button-stable', }
                      ]
                   });
            });
          }
      });
    }

   $scope.showCondicionesSorteo = function(condiciones){
          $translate(['ACEPTAR','PROMOCION']).then(function (msg) {
                 var alertPopup = $ionicPopup.alert({
                     title: msg.PROMOCION,
                     template: condiciones,
                     cssClass: 'stable',
                     buttons: [
                      { text: msg.ACEPTAR,
                        type: 'button-stable', }
                      ]
                   });
            });
   }

  
  $scope.showImage = function(index) {
    $scope.image = index;
    $scope.showModal('templates/image-popover.html');
  }
 
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
 
  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
  
  
})
.controller('AccountCtrl', function($scope,$rootScope,$ionicHistory,$translate,$ionicAnalytics,$cordovaGoogleAnalytics,$localstorage,$state,$ionicModal,$stateParams,$translate,$ionicLoading,$ionicPopup,$http, Complete_fields, myConfig) {
      if($rootScope.login==false){
        $state.go("promociones");
      }
      $ionicHistory.nextViewOptions({
          disableBack: true
      });
      if(typeof analytics !== 'undefined'){
        $cordovaGoogleAnalytics.trackView('Mi cuenta');
      }

      Complete_fields.get({lang:$rootScope.lang}).$promise.then(function(data) {
          $scope.complete_fields=data;
      });

      if($rootScope.user!=undefined){
        $rootScope.userfields=$rootScope.user.usuario_data;
      }
      $scope.switchLanguage = function(lang) {
        $translate.use(lang).then(function(){
          $rootScope.lang=lang;
          $localstorage.set("lang",lang);
          $rootScope.getMenu(); 
        })
      }

      $scope.eliminar_cuenta = function(){
          $translate(['eliminar_cuenta','seguro_eliminar_cuenta','cancel','estoy_seguro','cuenta_eliminada','Ok']).then(function (msg) {
            var confirmPopup = $ionicPopup.confirm({
               title: msg.eliminar_cuenta,
               template: msg.seguro_eliminar_cuenta,
               cssClass: 'stable',
               buttons:[{text:msg.cancel},{text:msg.estoy_seguro,onTap:function(e){
                return true;
               }}]
             });
             confirmPopup.then(function(res) {
               if(res) {//eliminar cuenta
                    var fd = new FormData();
                    fd.append('apikey',myConfig.APIKEY);
                    fd.append('iduser', $rootScope.user.iduser);
                    fd.append('token', $localstorage.get('token'));
                    $http.post(myConfig.URL+"/api/v1/eliminarCuenta.json", fd, {
                        transformRequest:angular.identity,
                        headers:{'Content-Type':undefined
                        }
                    }).success(function(data){
                          if(data.error==false){
                            $rootScope.logout();
                            $translate(['mi_cuenta','cuenta_eliminada','Ok']).then(function (msg) {
                               var alertPopup = $ionicPopup.alert({
                                   title: msg.mi_cuenta,
                                   template: msg.cuenta_eliminada,
                                   cssClass: 'stable',
                                   buttons: [
                                    { text: msg.Ok,
                                      type: 'button-stable' }
                                    ]
                                 });
                               $state.go('app.promociones');
                            });
                            
                          }
                    });
               }
             });
        });
      }
})
.controller('LoginCtrl', function($scope,$rootScope,$ionicHistory,$cordovaGoogleAnalytics,$sce,$ionicAnalytics,$cordovaGoogleAnalytics,$ionicHistory,$localstorage,$state,$ionicModal,$stateParams,$translate,$ionicLoading,$ionicPopup,$http, Page, myConfig) {
    $ionicHistory.nextViewOptions({
          disableBack: true
    });
    $scope.myConfig=myConfig;
    $scope.checkCondicionesPromokiss=0;

    function _waitForAnalytics(){
          if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.startTrackerWithId(myConfig.ANALYTICS);
              $cordovaGoogleAnalytics.trackView('Login | Registro');
              $cordovaGoogleAnalytics.addCustomDimension(1, $rootScope.lang);
              $cordovaGoogleAnalytics.addCustomDimension(2, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(3, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(4, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(5, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(6, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(7, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(8, "(not set)");
          }
          else{
              setTimeout(function(){
                  _waitForAnalytics();
              },250);
          }
      };
      _waitForAnalytics();
    
 
  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };

  $scope.doLogin = function(){
          var url=myConfig.URL+"/api/v1/usuario.json";
          $ionicLoading.show({
              content: 'Cargando',
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
          });
          var fd = new FormData();
          fd.append('emaila', loginForm.email.value);
          fd.append('passa', loginForm.password.value);
          fd.append('apikey',myConfig.APIKEY);
          fd.append('token', $localstorage.get('token'));
          //fd.append('recovery',loginForm.recovery.checked);
          $http.post(url, fd, {
              transformRequest:angular.identity,
              headers:{'Content-Type':undefined
              }
          }).success(function(data){
            data=JSON.parse(data);
            $ionicLoading.hide();
            if(data.error){
              $translate('LOGIN_INCORRECT').then(function (error_msg) {
                $rootScope.validation_errors=error_msg;
                $rootScope.validation=true;
              });
            }
            else{ 
              $rootScope.validation_errors="";
              $rootScope.validation=false;
              $ionicAnalytics.track('Login',true);
              if(typeof analytics !== 'undefined'){
                $cordovaGoogleAnalytics.trackEvent('Login', 'Logeado correctamente');
              }
              var user = {iduser:data.id,email:data.email,hashcode:data.hashcode,usuario_data:data.usuario_data};
              $rootScope.user=user;
              $rootScope.user.usuario_data=data.usuario_data;
              $localstorage.setObject('user',user);
              $rootScope.login=true;
              $state.go('app.promociones');
            }
          });
      }
      
      $scope.doRegister = function(){
          if(registerForm.password.value!=registerForm.password_repeat.value){
            $translate('PASSWORD_REPEAT_VALIDATION').then(function (error_msg) {
                $scope.validation_errors_reg=error_msg;
                $scope.validation_reg=true;
              });
            return false;
          }
          var url=myConfig.URL+"/api/v1/usuarioRegister.json";
          $ionicLoading.show({
              content: 'Cargando',
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
          });
          var fd = new FormData();
          fd.append('emaila', registerForm.email.value);
          fd.append('passa', registerForm.password.value);
          fd.append('apikey',myConfig.APIKEY);
          fd.append('token', $localstorage.get('token'));
          //fd.append('recovery',loginForm.recovery.checked);
          $http.post(url, fd, {
              transformRequest:angular.identity,
              headers:{'Content-Type':undefined
              }
          }).success(function(data){
              data=JSON.parse(data);
              $ionicLoading.hide();
              if(data.error){
                $translate(['Ok']).then(function (msg) {
                       var alertPopup = $ionicPopup.alert({
                           title: 'Promokiss',
                           template: data.error_msg,
                           cssClass: 'stable',
                           buttons: [
                            { text: msg.Ok,
                              type: 'button-stable' }
                            ]
                         });
                       $state.go('app.promociones');
                    });
              }
              else{
                $ionicAnalytics.track('Registro',true);
                if(typeof analytics !== 'undefined'){
                  $cordovaGoogleAnalytics.trackEvent('Registro', 'Registrado correctamente');
                }
                var user = {iduser:data.id,email:registerForm.email.value,hashcode:data.hashcode,usuario_data:data.usuario_data};
                $localstorage.setObject('user',user);
                $rootScope.user = user;
                $rootScope.login=true;
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });
                $state.go('app.promociones');
              }
          });
      }
})
.controller('PageCtrl', function($scope,$rootScope,$ionicAnalytics,$state,$stateParams,$translate,$ionicLoading, Page, myConfig) {
      $ionicLoading.show({
            content: 'Cargando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
      });
      Page.get({id:$stateParams.id,lang:$rootScope.lang}).$promise.then(function(data) {
        $scope.page=data;
        $ionicLoading.hide();
        function _waitForAnalytics(){
          if(typeof analytics !== 'undefined'){
              $cordovaGoogleAnalytics.startTrackerWithId(myConfig.ANALYTICS);
              $cordovaGoogleAnalytics.trackView($scope.page.title);
              $cordovaGoogleAnalytics.addCustomDimension(1, $rootScope.lang);
              $cordovaGoogleAnalytics.addCustomDimension(2, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(3, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(4, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(5, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(6, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(7, "(not set)");
              $cordovaGoogleAnalytics.addCustomDimension(8, "(not set)");
          }
          else{
              setTimeout(function(){
                  _waitForAnalytics();
              },250);
          }
      };
      _waitForAnalytics();
      });
      
})
