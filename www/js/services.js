angular.module('starter.services', ['ngResource'])
.factory('Promociones', function($resource) {
  // Might use a resource here that returns a JSON array
  return $resource('http://promokiss.com/api/v1/promociones.json?offset=:offset&limit=:limit&lang=:lang',{offset:'@offset',limit:'@limit',lang:'@lang'},
    {"all":{method:"GET",isArray:false}});
  
})
.factory('MisPromociones', function($resource) {
  // Might use a resource here that returns a JSON array
  return $resource('http://promokiss.com/api/v1/promociones.json?offset=:offset&limit=:limit&lang=:lang&apikey=:apikey&iduser=:iduser',{apikey:'@apikey',iduser:'@iduser',offset:'@offset',limit:'@limit',lang:'@lang'},
    {"all":{method:"GET",isArray:false}});
  
})
.factory('Promocion', function($resource) {
  return $resource('http://promokiss.com/api/v1/get.json?id=:id&lang=:lang',{id:'@id',lang:'@lang'},
    {"get":{method:"GET",isArray:false}});
  
})
.factory('PromocionURL', function($resource) {
  return $resource('http://promokiss.com/api/v1/get.json?url=:url&lang=:lang',{url:'@url',lang:'@lang'},
    {"get":{method:"GET",isArray:false}});
  
})
.factory('ReferAction', function($resource) {
  return $resource('http://promokiss.com/api/v1/refer.json',{action:'@action', referral:'@referral',idpromocion:'@idpromocion',apikey:'@apikey',lang:'@lang'},
    {"get":{method:"POST",isArray:false}});
  
})
.factory('MiPromocion', function($resource) {
  return $resource('http://promokiss.com/api/v1/get.json?id=:id&lang=:lang&iduser=:iduser',{id:'@id',lang:'@lang',iduser:'@iduser'},
    {"get":{method:"GET",isArray:false}});
  
})
.factory('Page', function($resource) {
  return $resource('http://promokiss.com/api/v1/page.json?id=:id&lang=:lang',{id:'@id',lang:'@lang'},
    {"get":{method:"GET",isArray:false}});
  
})
.factory('Menu', function($resource) {
  return $resource('http://promokiss.com/api/v1/menu.json?lang=:lang',{lang:'@lang'},
    {"get":{method:"GET",isArray:true}});
  
})
.factory('Complete_fields', function($resource) {
    return $resource('http://promokiss.com/api/v1/complete_fields.json?lang=:lang',{lang:'@lang'},
    {"get":{method:"GET",isArray:false}});
})
 
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {

      return JSON.parse($window.localStorage[key] || '{}');
    },
    remove:function(key){
      $window.localStorage.removeItem(key);
    }
  }
}])