// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core','ionic.service.push','ionic.service.analytics','starter.controllers', 'starter.services','pascalprecht.translate','ngCordova',"ionic-multiselect",'ionicLazyLoad','ion-autocomplete'])

.run(function($ionicPlatform, $cordovaGoogleAnalytics, $ionicAnalytics, $state, $localstorage,$http,$ionicPush) {
  $ionicPlatform.ready(function() {
    var APIKEY="debug";
    var URL="http://promokiss.com";
    $ionicPush.init({
      "onNotification":function(notification){
        //controlamos las notificaciones push que lleguen al dispositivo
        if(notification.payload.Id!=undefined){
           $state.go("app.promocion-detail",{"Id":notification.payload.Id});
        }
        
      },
      "onRegister":function(token){
          
          //actualizamos la información que tenemos del dispositivo
          var fd = new FormData();
          fd.append('token', token.token);
          fd.append('apikey',APIKEY);
          fd.append('platform',ionic.Platform.platform());
          fd.append('uuid',ionic.Platform.device().uuid);
          fd.append('version',ionic.Platform.device().version);
          fd.append('model',ionic.Platform.device().model);
          fd.append('ultimoAcceso',Date.now());
          var url=URL+"/api/v1/usuarioRegisterToken.json";
          $http.post(url, fd, {transformRequest:angular.identity,headers:{'Content-Type':undefined}}).success(function(data){}).error(function(error){alert(error)});
          $localstorage.set("token",token.token);
      }
    });
    $ionicPush.register();
    $ionicAnalytics.register();
   
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
     
    }
    // controlamos los links del exterior que se abren con promokiss.
    universalLinks.subscribe('openPromo', openPromo);
    universalLinks.subscribe('openExt', openExt);
    function openPromo(event) {
        var path = event.path.split("/"); //obtenemos el path de la url que abrimos, debería de ser del tipo /lang/nombre-promocion, si es así abrimos la promoción
        if(path.length>1){
           var url = path[(path.length-1)];
           var lang= path[(path.length-2)];
           $state.go("app.promocion-detail-url",{"url":url,"lang":lang});
        }
        else{
          $state.go("app.promociones");
        }
    };
    function openExt(event) {
        window.open(URL+event.path, '_system');
    };


    

    
  });
})
.config(function($ionicConfigProvider, $stateProvider,
    $urlRouterProvider, $translateProvider) {

    $translateProvider
      .useStaticFilesLoader({
        prefix: 'locales/',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['es', 'eu'], {
        'es' : 'es',
        'eu' : 'eu',
      })
      .preferredLanguage('es')
      .fallbackLanguage('es')
      .determinePreferredLanguage()
      .useSanitizeValueStrategy('escapeParameters');
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })

  // Each tab has its own nav history stack:

  .state('app.promociones', {
      url: '/promociones',
      views: {
        'menuContent': {
          templateUrl: 'templates/promociones.html',
          controller: 'PromocionesCtrl'
        }
      }
    })
  .state('app.mispromociones', {
      url: '/mispromociones',
      views: {
        'menuContent': {
          templateUrl: 'templates/mispromociones.html',
          controller: 'MisPromocionesCtrl'
        }
      }
    })
    .state('app.mipromocion-detail', {
      url: '/mispromociones/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/promocion-detail.html',
          controller: 'MiPromocionDetailCtrl'
        }
      }
    })
    .state('app.promocion-detail', {
      url: '/promociones/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/promocion-detail.html',
          controller: 'PromocionDetailCtrl'
        }
      }
    })
    .state('app.promocion-detail-url', {
      url: '/promociones/:lang/:url',
      views: {
        'menuContent': {
          templateUrl: 'templates/promocion-detail.html',
          controller: 'PromocionDetailCtrl'
        }
      }
    })
    .state('app.account', {
      url: '/account',
      views: {
        'menuContent': {
          templateUrl: 'templates/account.html',
          controller: 'AccountCtrl'
        }
      }
    })
    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
    .state('app.page', {
        url: '/page/:id/0/:url',
        views: {
          'menuContent': {
            templateUrl: 'templates/page.html',
            controller: 'PageCtrl'
          }
        }
    })
    
    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/promociones');
})
.config(function($ionicConfigProvider) {
     $ionicConfigProvider.backButton.text('');
     $ionicConfigProvider.backButton.previousTitleText(false);
    //$ionicConfigProvider.tabs.position("bottom"); 
    $ionicConfigProvider.platform.android.tabs.position('standard');
    $ionicConfigProvider.views.maxCache(1);
})
.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            if ( !attrs.href ){
                return;
            }
            var externalRe = new RegExp("^(http|https)://");
            var url = attrs.href;

            if(externalRe.test(url)) {
                element.on('click',function(e){
                    e.preventDefault();
                    if(attrs.ngClick){
                        scope.$eval(attrs.ngClick);
                    }
                    window.open(encodeURI(url), '_system');
                });
            }
        }
    };
})
.directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
   )};
}])
.directive('passwordrepeat', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, mCtrl) {
      function passwordrepeatvalidation(value) {
        if(value=='' || scope.registerForm.password.$viewValue!=value){
          mCtrl.$setValidity('password_repeat',false);
        }
        else{
           mCtrl.$setValidity('password_repeat',true);
        }
        return value;
      }
      mCtrl.$parsers.push(passwordrepeatvalidation);
    }
  };
})
.directive('mypasswordrepeat', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, mCtrl) {
      function passwordrepeatvalidation(value) {
        if(value=='' || scope.accountForm.password.$viewValue!=value){
          mCtrl.$setValidity('password_repeat',false);
        }
        else{
           mCtrl.$setValidity('password_repeat',true);
        }
        return value;
      }
      mCtrl.$parsers.push(passwordrepeatvalidation);
    }
  };
})
.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                scope.fields[attrs.field]=files[0];
                document.getElementById("files_"+attrs.field).innerHTML=files[0].name;  
            });
        }
    };
})


.constant("myConfig",{
  //definimos constantes que se usan en los controladores. Si cambiamos alguna de estas, verificar que se corresponden con las que hay en app.js y services.js
  "URL":"http://promokiss.com",
  "URL_CONDICIONES":"http://promokiss.com/condiciones_promokiss.html",
  "APIKEY":"debug",
  "ANALYTICS":'UA-54855223-2'
})
